// document.addEventListener('keydown', function (e) {
//   if (e.code === 'escape') {
//     const popupActive = document.querySelector('.popup.open');
//   }
// })

export default class Popup {
  constructor({ triggerSelector, popupId, closeSelector }) {
    this.trigger = document.querySelectorAll(triggerSelector);
    this.closeCross = document.querySelectorAll(closeSelector);
    this.popup = document.querySelector(popupId);
    this.body = document.querySelector('body');
    this.#triggerAddEvent();
  }

  #triggerAddEvent = () => {
    for (let i = 0; i < this.trigger.length; i++) {
      this.trigger[i].addEventListener('click', (e) => {
        this.#popupOpen(this.popup);
        e.preventDefault();
      })
    }
  }

  #closeButton = () => {
    for (let i = 0; i < this.closeCross.length; i++) {
      this.closeCross[i].addEventListener('click', (e) => {
        this.#popupClose(this.popup);
        e.preventDefault();
      })
    }
  }

  #isOpen = () => {
    const popupActive = document.querySelector('.popup.open');
    if (popupActive) {
      this.#popupClose(popupActive);
    }
  }
  #popupOpen = (Popup) => {
      this.#isOpen();
      this.#bodyLock();
      if (!Popup.classList.contains('popup')) Popup.classList.add('popup');
      Popup.classList.add('open');
      Popup.addEventListener('click', (e) => {
        if (!e.target.closest('.popup__body')) {
          this.#popupClose(e.target.closest('.popup'));
        }
      });
      this.#closeButton();
    }

  #popupClose = (popupActive) => {
      popupActive.classList.remove('open');
      this.#bodyUnLock();
    }

  #bodyLock = () => {
      const selectorBody=document.querySelector('body')
      // const selectorHtml=document.querySelector('html')
      // const selectorPopup=document.querySelectorAll('.popup')
      const popupBody = this.popup.querySelector('.popup__body')
      // debugger;
      // const widthScroll = this.#widthScroll() + 'px';
      // debugger;
      this.body.style.width = selectorBody.clientWidth + 'px';
      popupBody.style.marginLeft = "-" + (this.#widthScroll() + 'px');
      // debugger;
      this.body.classList.add('lock');
    }
  #bodyUnLock = () => {
      this.body.style.width = '';
      // this.body.style.paddingRight = '';
      document.querySelector('html').style.paddingRight = '';
      this.body.classList.remove('lock');
    }

  #widthScroll =() => {
      let div = document.createElement('div');
      div.style.overflowY = 'scroll';
      div.style.width = '50px';
      div.style.height = '50px';
      div.style.visibility = 'hidden';
      document.body.appendChild(div);
      var scrollWidth = div.offsetWidth - div.clientWidth;
      document.body.removeChild(div);
      return scrollWidth;
  }
  
  }
