export default function () {
  document.addEventListener('DOMContentLoaded', () => {

    let burger_button = document.querySelector(".burger_button");
    let menu = document.querySelector(".header__menu");
    let link = menu.querySelectorAll("a");

    burger_button.addEventListener("click", () => {
      if (burger_button.classList.contains("active")) {
        burger_button.classList.remove("active");
        menu.classList.remove("burger-active", "overlay");
        document.body.classList.remove("no-scroll");
      }
      else {
        burger_button.classList.add("active");
        menu.classList.add("burger-active", "overlay");
        document.body.classList.add("no-scroll");
        querySelector(".header").style.overflow = 'auto';
      }
    })
    link.forEach((linkItem) => {
      linkItem.addEventListener("click", () => {
        if (burger_button.classList.contains("active")) {
          burger_button.classList.remove("active");
          menu.classList.remove("burger-active", "overlay");
          document.body.classList.remove("no-scroll");
        }
      })
    })
  })
}