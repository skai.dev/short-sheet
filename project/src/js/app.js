// import Popup from "./additions/popup";
// import burger_menu from "./additions/burger_menu";
// import smooth_scrolling from "./additions/smooth_scrolling";
// import Splide from '@splidejs/splide';

burger_menu();

smooth_scrolling('a[href^="#"]');
new Splide('.slider-photo', {
  type: 'loop',
  speed: 1000,
  padding: 0,
  trimSpace: false,
  // clones: 2,
  autoplay: true,
  width: 'auto',
  height: '235px',
  cover: true,
  breakpoints: {
      1000: {
          perPage: 2
      },
      700: {
          perPage: 1
      }
  },
  focus: 1,
  perPage: 3,
  pagination: false,
  gap: '2%',
  arrowPath: ''

}).mount();



// const popup = new Popup({
// 	triggerSelector: '.trigger-popup-review',
// 	popupId: '#popup-review',
// 	closeSelector: '.popup__close'
// })