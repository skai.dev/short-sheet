
let { src, dest, series, watch, parallel } = require('gulp');
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const bss = require("browser-sync").create();
// const sass = require("gulp-dart-sass");
const sass = require("gulp-sass")(require('sass'));
const autoprefixer = require("gulp-autoprefixer");
const webpack = require('webpack-stream')
const rename = require("gulp-rename");
const newer = require('gulp-newer');
const gulpPug = require('gulp-pug');
const bemValidator = require('gulp-html-bem-validator');
const cleanCss = require('gulp-clean-css');
const SVGSprite = require('gulp-svg-sprite');
const filter = require('gulp-filter');
const sourcemaps = require('gulp-sourcemaps');
const htmlhint = require("gulp-htmlhint");
const pxtorem = require('gulp-pxtorem');
const webp = require('gulp-webp');
const ttf2woff2 = require('gulp-ttf2woff2');
const responsiveConfig = require('gulp-responsive-config');
const responsive = require('gulp-responsive');
const svgmin = require('gulp-svgmin');

let param = {
    source: 'project/src',
    prod: 'project/prod',
    options: {
        devmode: 'development',
        levelCleanCss: 0,
    }
}

function browsersync() {
    bss.init({
        server: {
            baseDir: param.prod,
        },
        notify: true,
        online: true
    })
}

function pug() {
    switch (param.options.devmode) {
        case 'development': {
            return src([param.source + '/pug/index.pug'])
                .pipe(gulpPug({ pretty: true }))
                .pipe(rename('index.html'))
                .pipe(bemValidator())
                .pipe(htmlhint())
                .pipe(htmlhint.reporter())
                .pipe(dest(param.prod))
                .pipe(bss.stream())
        }
        case 'production': {
            return src([param.source + '/pug/index.pug'])
                .pipe(gulpPug({ pretty: true }))
                .pipe(rename('index.html'))
                .pipe(dest(param.prod))
        }
    }
}

function html() {
    switch (param.options.devmode) {
        case 'development': {
            return src([param.source + '/**/*.html'])
                .pipe(bemValidator())
                .pipe(htmlhint())
                .pipe(dest(param.prod))
                .pipe(bss.stream())
        }
        case 'production': {
            return src([param.source + '/**/*.html'])
                .pipe(dest(param.prod))
        }
    }
}

function styles() {
    switch (param.options.devmode) {
        case 'development': {
            return src(param.source + '/sass/main.sass')
                .pipe(sourcemaps.init())
                .pipe(sass({ outputStyle: 'expanded' }).on("error", sass.logError))
                .pipe(sourcemaps.write())
                .pipe(rename('style.min.css'))
                .pipe(dest(param.prod + '/css'))
                .pipe(bss.stream())
        }
        case 'production': {
            return src(param.source + '/sass/main.sass')
                .pipe(sass({ outputStyle: 'compressed' }).on("error", sass.logError))
                .pipe(pxtorem({
                    rootValue: 16,
                    unitPrecision: 5,
                    propList: ['*', '!border*'],
                    selectorBlackList: [],
                    replace: true,
                    mediaQuery: false,
                    minPixelValue: 0,
                }, {}))
                .pipe(autoprefixer({ grid: true }))
                .pipe(cleanCss({ level: 2 }))
                .pipe(rename('style.min.css'))
                .pipe(dest(param.prod + '/css'))
        }
    }
}

function scripts() {
    return src(param.source + '/js/app.js')
        .pipe(webpack({
            mode: param.options.devmode,
            module: {
                rules: [
                    {
                        test: /\.(js)$/,
                        exclude: /(node_modules)/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['@babel/env'],
                            plugins: ['babel-plugin-root-import',
                                '@babel/plugin-proposal-class-properties']
                        }
                    }
                ]
            }
        })).on('error', function handleError() { this.emit('end') })
        .pipe(rename('app.min.js'))
        .pipe(dest(param.prod + '/js'))
        .pipe(bss.stream())
}

function convertWebp() {
    return src(param.source + '/img/**/*.{jpg,jpeg,png}')
        .pipe(webp())
        .pipe(dest(param.source + '/img'));
}

function destSvg() {
    return src(param.source + '/img/**/*.svg')
        .pipe(filter(['**', '!' + param.source + '/img/{sprite,sprite/**/*}']))
        // .pipe(imagemin())
        .pipe(svgmin())
        .pipe(gulp.dest(param.prod + '/img'));
}

function images(cb) {
    convertWebp();
    destSvg();
    switch (param.options.devmode) {
        case 'development': {
            return src(param.source + '/img/**/*')
                .pipe(filter(['**', '!' + param.source + '/img/{sprite,sprite/**/*}']))
                .pipe(newer(param.prod + '/img'))
                .pipe(responsive(responsiveConfig([param.prod + '/**/*.html']), { errorOnUnusedConfig: false, errorOnUnusedImage: false, silent: true, stats: false }))
                .pipe(dest(param.prod + '/img'));
        }
        case 'production': {
            return src(param.source + '/img/**/*')
                .pipe(filter(['**', '!' + param.source + '/img/{sprite,sprite/**/*}']))
                .pipe(responsive(responsiveConfig([param.prod + '/**/*.html'])))
                .pipe(dest(param.prod + '/img'));

        }
    }
}

function SvgSprite() {
    return gulp.src(param.source + '/img/sprite/*.svg')
        .pipe(SVGSprite({
                mode: {
                    stack: {
                        sprite: "../sprite.svg"
                    }
                },
            }
        ))
        .pipe(imagemin())
        .pipe(gulp.dest(param.prod + '/img'));
}

function Woff2() {
    return src([param.source + '/fonts/**/*.woff2'])
        .pipe(newer(param.prod + '/fonts'))
        .pipe(dest(param.prod + '/fonts'))
}

function fonts() {
    return src([param.source + '/fonts/**/*.{ttf,woff,woff2}'])
        .pipe(newer(param.prod + '/fonts'))
        .pipe(ttf2woff2())
        .pipe(dest(param.prod + '/fonts'))
        .pipe(bss.stream())
}

function startwatch() {
    watch([param.source + '/sass/**/*.{sass,scss}'], { usePolling: true }, styles);
    watch([param.source + '/**/*.html'], { usePolling: true }, series(html, images));
    watch([param.source + '/**/*.pug'], { usePolling: true }, series(pug, images));
    watch([param.source + '/js/**/*.js'], { usePolling: true }, scripts)
    watch(param.source + '/images/**/*.{jpg,jpeg,png,webp,svg,gif}', { usePolling: true }, parallel(images, SvgSprite));
    watch(param.source + '/fonts/**/*', { usePolling: true }, fonts);
    // watch(`app/**/*.{${fileswatch}}`, { usePolling: true }).on('change', browserSync.reload)
}


function production(cb) {
    param.options.devmode = 'production';
    productionBuild();
    cb();
}

exports.production = production;
let productionBuild = series(pug, html, styles, scripts, parallel(images, fonts, SvgSprite), parallel(browsersync, startwatch));
exports.default = series(pug, html, styles, scripts, parallel(images, fonts, SvgSprite), parallel(browsersync, startwatch));

